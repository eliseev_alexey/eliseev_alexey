package New;

/**
 * Created by Alexey-PC on 23.05.2016.
 */
public class mix {

    /**
     * Created by Alexey-PC on 07.05.2016.
     */
    public static void main(String[] args) {
        System.out.println(solution(1, 123000));
    }

    private static int solution(int a, int b) {
        int aCount = 1;
        int bCount = 1;
        int abCount;
        int a2;
        int result = 0;

        int razryad = 10;
        while (a / razryad > 0) {
            razryad *= 10;
            aCount++;
        }
        razryad = 10;
        while (b / razryad > 0) {
            razryad *= 10;
            bCount++;
        }
        abCount = aCount + bCount;
        for (; abCount >= 1; ) {
            if (aCount >= 1) {
                a2 = (int) Math.pow(10, aCount);
                result = result + a % a2 / (a2 / 10) * (int) Math.pow(10, abCount) / 10;
                aCount--;
                abCount--;
                // hibivfv
            }
            if (bCount >= 1) {
                a2 = (int) Math.pow(10, bCount);
                result = result + b % a2 / (a2 / 10) * (int) Math.pow(10, abCount) / 10;
                bCount--;
                abCount--;
            }
        }
        return result;
    }
}
