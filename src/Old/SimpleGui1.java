package Old;

import javax.swing.*;

/**
 * Created by Alexey-PC on 20.03.2016.
 */
public class SimpleGui1 {
    public static void main (String[] args) {
        JFrame frame = new JFrame();
        JButton button = new JButton("click me");

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.getContentPane().add(button);

        frame.setSize(300,300);

        frame.setVisible(true);
    }
}
